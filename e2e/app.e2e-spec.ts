import { TejaaslabsPage } from './app.po';

describe('tejaaslabs App', () => {
  let page: TejaaslabsPage;

  beforeEach(() => {
    page = new TejaaslabsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
