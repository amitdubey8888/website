import { Component } from '@angular/core';

@Component({
  selector: `app-root`,
  templateUrl: `./app.component.html`,
  styleUrls: [`./app.component.css`]
})
export class AppComponent {

  private navItems = [
    { name: `Home`, link: `#home` },
    { name: `Services`, link: `#services` },
    { name: `Technologies`, link: `#skills` },
    { name: `Our Team`, link: `#team` },
    { name: `Contact`, link: `#contact` },
  ];

  private ourServices = [
    {
      name: `Mobile Applications`,
      type: [
        {
          name: `Android Apps`,
          icon: `fa fa-android`,
          color: `tl-icon-android`
        },
        {
          name: `iOS Apps`,
          icon: `fa fa-apple`,
          color: `grey-text darken-3`
        },
        {
          name: `Windows Apps`,
          icon: `fa fa-windows`,
          color: `blue-text lighten-2`
        }
      ]
    },
    {
      name: `Web Applications`,
      type: [
        {
          name: `E-commerce`,
          icon: `fa fa-shopping-cart`,
          color: `pink-text darken-1`
        },
        {
          name:  `CMS`,
          icon: `fa fa-cogs`,
          color: `red-text darken-3`
        },
        {
          name: `Dynamic`,
          icon: `fa fa-refresh`,
          color: `purple-text darken-3`
        },
        {
          name: `Static`,
          icon: `fa fa-html5`,
          color: `indigo-text darken-3`
        }
      ]
    },
    {
      name: `Other Services`,
      type: [
        {
          name: `Server Side Solution`,
          icon: `fa fa-server`,
          color: `light-blue-text`
        },
        {
          name: `UI/UX Design`,
          icon: `fa fa-paint-brush`,
          color: `teal-text darken-1`
        },
        {
          name: `Testing`,
          icon: `fa fa-search`,
          color: `green-text darken-1`
        },
        {
          name: `SEO & SMO`,
          icon: `fa fa-line-chart`,
          color: `deep-orange-text darken-2`
        }
      ]
    }
  ];

  private ourSkills = [
    {
      name: `Front-End Development`,
      type: [
        {
          name: `Angular 2+`,
          image: `assets/images/skills/angular.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `Ionic 2+`,
          image: `assets/images/skills/ionic.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `Javascript (ES6)`,
          image: `assets/images/skills/js.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `HTML 5`,
          image: `assets/images/skills/html.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        }
      ]
    },
    {
      name: `Back-End Development`,
      type: [
        {
          name: `NodeJS`,
          image: `assets/images/skills/node.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `Php`,
          image: `assets/images/skills/php.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `Python`,
          image: `assets/images/skills/python.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `Loopback`,
          image: `assets/images/skills/loopback.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        }
      ]
    },
    {
      name: `Databases`,
      type: [
        {
          name: `MongoDB`,
          image: `assets/images/skills/mongodb.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `MySQL`,
          image: `assets/images/skills/mysql.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `Firebase`,
          image: `assets/images/skills/firebase.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `PostgreSQL`,
          image: `assets/images/skills/postgresql.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        }
      ]
    },
    {
      name: `UI/UX Design`,
      type: [
        {
          name: `CSS 3`,
          image: `assets/images/skills/css.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `Material Design`,
          image: `assets/images/skills/material.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `Materialize CSS`,
          image: `assets/images/skills/materializecss.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        },
        {
          name: `Bootstrap`,
          image: `assets/images/skills/bootstrap.png`,
          quote: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris 
          mattis placerat nulla et porta. Nulla facilisi. Nulla pharetra mi nunc, 
          mattis laoreet augue rhoncus quis. Maecenas tincidunt arcu nec tellus auctor porta.`
        }
      ]
    }


  ];

  private teamMembers = [
    {
      name: `Shubham Jaiswal`,
      image: `assets/images/team/shubham.jpg`,
      fb: ``,
      twitter: ``,
      instagram: ``,
      linkedin: ``,
      position: `Full-Stack Developer`,
      about: `I'm an ultimate freak, live in another dimension and too moody, can't help it!`
    },
    {
      name: `Amit Dubey`,
      image: `assets/images/team/amit.jpg`,
      fb: `https://www.facebook.com/amitdubey1993`,
      twitter: `https://twitter.com/amitdubey8888`,
      instagram: `https://www.instagram.com/amitdubey1993`,
      linkedin: `https://www.linkedin.com/in/amitdubey1993`,
      position: `Full-Stack Developer`,
      about: `I'm a person living in own world. I'm a self taught person,
      can work on any technology. `
    },
    {
      name: `Anuj Rana`,
      image: `assets/images/team/anuj.jpg`,
      fb: `https://www.facebook.com/anuj.rana.2813`,
      twitter: `https://twitter.com/anujrana2813`,
      instagram: `https://www.instagram.com/anuj_rana_2813`,
      linkedin: `https://www.linkedin.com/in/anujrana2813`,
      position: `Marketing Head`,
      about: `I love to travel, love to touch in social media.I like to meet new 
      people.
      `
    },
    {
      name: `Pankaj Jangir`,
      image: `assets/images/team/pankaj.jpg`,
      fb: `https://www.facebook.com/Pankaj62473`,
      twitter: `https://twitter.com/anujrana2813`,
      instagram: `https://www.instagram.com/pankaj_jangir121/`,
      linkedin: `https://www.linkedin.com/in/pankaj-jangir-b60022146/`,
      position: `Front-End Developer`,
      about: `Sometime, Life is about risking everything for a dream no one can see but i can.`
    },
    {
      name: `Ayush Saxena`,
      image: `assets/images/team/ayush.jpg`,
      fb: ``,
      twitter: ``,
      instagram: ``,
      linkedin: `https://www.linkedin.com/in/ayush-profy/`,
      position: `Full-Stack Developer`,
      about: `Tech enthusiastic, more practical, quick learner, love to code web application.`
    },
    {
      name: `Jai Rana`,
      image: `assets/images/team/jai.jpg`,
      fb: `https://www.facebook.com/konkanakalan`,
      twitter: `https://twitter.com/jkrana008`,
      instagram: `https://www.instagram.com/jai_rana_official/`,
      linkedin: `https://www.linkedin.com/in/jai-rana/`,
      position: `Full-Stack Developer`,
      about: `There is few words that describe me. I am a member of great team.`
    },

  ];



  constructor() {
  }
  
}
